package main

import (
	"log"
	"net/http"
	
	"github.com/PuerkitoBio/goquery"
)

func main() {
	// Request the HTML page.
	res, err := http.Get("https://go101.org/article/101.html")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Find the review items
	doc.Find("li a").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		title, _ := s.Attr("href")
		log.Printf("%d: %s\n", i, title)
	})
}
